%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:          sos
Version:       3.9.1
Release:       2
Summary:       A set of tools to gather troubleshooting information from a system
License:       GPLv2+
URL:           https://github.com/sosreport/sos
Source0:       https://github.com/sosreport/sos/archive/%{version}.tar.gz
BuildRequires: python3-devel gettext python3-six
Requires:      libxml2-python3 bzip2 xz python3-six
Conflicts:     vdsm <= 4.30.17
BuildArch:     noarch

%description
Sos is an extensible, portable, support data collection tool primarily
aimed at Linux distributions and other UNIX-like operating systems.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install '--install-scripts=%{_sbindir}'
install -Dm644 %{name}.conf %{buildroot}%{_sysconfdir}/%{name}.conf

%find_lang %{name} || echo 0

%files -f %{name}.lang
%license LICENSE
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sos.conf
%{_sbindir}/sosreport
%{python3_sitelib}/*

%files help
%defattr(-,root,root)
%doc AUTHORS README.md
%{_mandir}/man1/sosreport.1.gz
%{_mandir}/man5/sos.conf.5.gz

%changelog
* Mon Jun 1 2020 chengzihan <chengzihan2@huawei.com> - 3.9.1-2
- Package update

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.6-5
- Package init
